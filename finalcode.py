import tkinter as tk
import cv2
from PIL import Image, ImageTk
from tkinter import PhotoImage
import pygame
root = tk.Tk()
root.title("video")
go = PhotoImage(file = 'k.png')
video_label = tk.Label(root)
video_label.pack()
pygame.mixer.init()
video_path = "vid.mp4"
cap = cv2.VideoCapture(video_path)
zoom_factor = 2.36 
def update_video():
    global zoom_factor
    ret, frame = cap.read()
    if ret:
        zoomed_frame = cv2.resize(frame, None, fx=zoom_factor, fy=zoom_factor, interpolation=cv2.INTER_LINEAR)
        frame_rgb = cv2.cvtColor(zoomed_frame, cv2.COLOR_BGR2RGB)
        img = Image.fromarray(frame_rgb)
        img_tk = ImageTk.PhotoImage(image=img)
        video_label.img_tk = img_tk
        video_label.config(image=img_tk)
        if not pygame.mixer.music.get_busy():
            # Play audio if not already playing
            audio_path = 'audio.mp3'
            pygame.mixer.music.load(audio_path)
            pygame.mixer.music.play()
        video_label.after(10, update_video)  # Update every 10 ms
    else:
        cap.release()
dirs = [(1, 0), (0, 1), (-1, 0), (0, -1)]
def is_valid(x, y, m, n) -> bool:
    return (0 <= x < m) and (0 <= y <n)
def check(x, y, steps, m, n, checkpoint, t, visited,markpoints) -> int:     
    if not is_valid(x, y, m, n) or visited[x][y]:
        return 0
    if steps == t[3]:
        if (x, y) == checkpoint[3]:
            markpoints.append((x,y))
            return 1
    if steps == t[2]:
        if (x, y) != checkpoint[2]:
            return 0
    elif steps == t[1]:
        if (x, y) != checkpoint[1]:
            return 0
    elif steps == t[0]:
        if (x, y) != checkpoint[0]:
            return 0
    else:
        for i in range(4):
            if (x, y) == checkpoint[i]:
                return 0
            dist = abs(x - checkpoint[i][0]) + abs(y - checkpoint[i][1])
            left = t[i] - steps
            if 0 < left < dist:
                return 0
    visited[x][y] = True
    markpoints.append((x,y))
    path=0
    for i, j in dirs:
        next_x, next_y = x + i, y + j
        path += check(next_x, next_y, steps + 1, m, n, checkpoint, t, visited,markpoints)
    visited[x][y] = False
    return path
def solve_grid_problem(m, n, t, checkpoint) -> int:
    global markpoints
    markpoints = []
    visited = [[False for k in range(n)] for k in range(m)]
    return check(0, 0, 1, m, n, checkpoint,t, visited,markpoints)
def secpage():
    root.destroy()
    root2=tk.Tk()
    root2.title('GRID DETAILS')
    root2.geometry('1600x1600')
    bg = PhotoImage(file = 'bg2.png')
    bg3 = bg.zoom(3,3)
    my_label = tk.Label(root2, image = bg3).place(x = 0,y= 0)
    def appear_rows():
        alpha = 0  # Initial alpha (transparency)
        rowsl.place(x=400, y=200)  # Specific position for the first label (x=300, y=200)
        rowse.place(x=600, y=200)  # Specific position for the second label (x=500, y=200)
        def animate_rows():
            nonlocal alpha
            if alpha < 1:
                rowsl.configure(fg ='#191970', bg = '#A7C7E7')
                rowse.configure(fg ='#191970', bg = '#A7C7E7')
                alpha += 0.01  # Adjust the animation speed here
                root2.after(10, animate_rows)
            else:
                alpha = 1  # Set alpha to 1 when animation completes (fully opaque)
        animate_rows()
    def appear_columns():
        alpha = 0  # Initial alpha (transparency)
        coll.place(x=400, y=300)  # Specific position for the first label (x=300, y=200)
        cole.place(x=600, y=300)  # Specific position for the second label (x=500, y=200)
        def animate_columns():
            nonlocal alpha
            if alpha < 1:
                coll.configure(fg ='#191970', bg = '#A7C7E7')
                cole.configure(fg ='#191970', bg = '#A7C7E7')
                alpha += 0.01  # Adjust the animation speed here
                root2.after(10, animate_columns)
            else:
                alpha = 1  # Set alpha to 1 when animation completes (fully opaque)
            animate_columns()     
    def appear_ck():
        alpha = 0  # Initial alpha (transparency)A
        ckl.place(x=350, y=400)  # Specific position for the first label (x=300, y=200)
        cke.place(x=600, y=400)  # Specific position for the second label (x=500, y=200)
        def animate_ck():
            nonlocal alpha
            if alpha < 1:
                ckl.configure(fg ='#191970', bg = '#A7C7E7')
                cke.configure(fg ='#191970', bg = '#A7C7E7')
                alpha += 0.01  # Adjust the animation speed here
                root2.after(10, animate_ck)
            else:
                alpha = 1  # Set alpha to 1 when animation completes (fully opaque)
            animate_ck()         
    def solve1():
        global m,n,t,checkpoint
        m = int(rowse.get())
        n = int(cole.get())
        if (m !=0) and (n!= 0):
            a = list(map(int, cke.get().split()))  # Read the checkpoints (x, y)
            checkpoint= [(a[i], a[i+1]) for i in range(len(a)-1) if i % 2 == 0]
            checkpoint.append((0, 1))
            t = [m * n // 4, m * n // 2, 3 * m * n // 4, m * n]
            result = solve_grid_problem(m, n, t, checkpoint)  
            if result ==2:
                root2.withdraw()
                def mark_points(canvas, m, n, points, cell_size=20, steps=1):
                    if not points:
                        return
                    x, y = points[0]
                    canvas.create_rectangle(y * cell_size, (m-1-x) * cell_size, (y + 1) * cell_size, (m-x) * cell_size, fill="light blue")
                    canvas.create_text((y + 0.5) * cell_size, (m - 0.5 - x) * cell_size, text=str(steps), font=("Arial", 12), fill="black")
                    canvas.after(1000, mark_points, canvas, m, n, points[1:], cell_size, steps + 1)   
                def show_grid_with_points(canvas, m, n, cell_size=20, i=0, j=0):
                    if i >= m:
                        return
                    canvas.create_rectangle(j * cell_size, (m-1-i) * cell_size, (j + 1) * cell_size, (m-i) * cell_size, fill="#6495ED")
                    next_i = i
                    next_j = j + 1
                    if next_j >= n:
                        next_i = i + 1
                        next_j = 0
                    canvas.after(100, show_grid_with_points, canvas, m, n, cell_size, next_i, next_j)
                def mark_checkpoints(canvas, checkpoint_positions):
                    if not checkpoint_positions:
                        return
                    row, col = checkpoint_positions[0]
    # Reverse the row calculation to adjust for the reversed y-axis
                    row =m - row
                    canvas.create_rectangle(col * cell_size, row * cell_size,(col + 1) * cell_size, (row + 1) * cell_size,outline="red", width=2)
                    canvas.after(2000, mark_checkpoints, canvas, checkpoint_positions[1:])   
                root3 = tk.Tk()
                root3.title("Grid with Marked Points")
                root3.geometry('1500x1500')
                root3.configure(bg='#6F8FAF')
                m, n = 3, 6  # Replace this with your desired grid size
                cell_size = 100  # Adjust the size of the cells in the grid here
                def create_list_until_element_found(list1, target_element):
                    list2 = []
                    for element in list1:
                        if element != target_element:
                            list2.append(element)
                        else:
                            list2.append((0,1))   
                            break 
                    return list2
                list1 = markpoints
                target_element = (0,1)
                list2 = create_list_until_element_found(list1, target_element)
                points_to_mark_1 = list2
                points_to_mark_2 = [(0,0),(1,0),(2,0),(2,1),(1,1),(1,2),(2,2),(2,3),(2,4),(2,5),(1,5),(0,5),(0,4),(1,4),(1,3),(0,3),(0,2),(0,1)]
                canvas1 = tk.Canvas(root3, bg='#6F8FAF',width=n * cell_size, height=m * cell_size)
                canvas1.pack(side=tk.LEFT, padx=50, pady=20)   # Adjust the coordinates to position the canvas
                show_grid_with_points(canvas1, m, n, cell_size)
                root3.after(3000, lambda: mark_points(canvas1, m, n, points_to_mark_1, cell_size))  # Use lambda
                canvas2 = tk.Canvas(root3, bg='#6F8FAF', width=n * cell_size, height=m * cell_size)
                canvas2.pack(side=tk.LEFT, padx=50, pady=20)  # Adjust the coordinates to position the canvas
                def close():
                    root3.destroy()
                bttn1 = tk.Button(root3,text ='close',font =('arial',30),fg = 'black',bg ='#6F8FAF' ,command = close)
                bttn1.place(x =600, y =600)
                l1 = tk.Label(root3,text = ('Valid Paths Travelled'),font = ('gariola',60) ,fg = 'black',bg = '#6F8FAF')
                l1.place(x = 350,y = 40)
                def start_canvas2():
                    show_grid_with_points(canvas2, m, n, cell_size)
                    root3.after(3000, lambda: mark_points(canvas2, m, n, points_to_mark_2, cell_size))  # Use lambda    
                root3.after(20000, start_canvas2)  # Start canvas2 after canvas1 is done    
                root3.mainloop()
            else:
                root2.withdraw()
                def slide_label(label, x):
                    x -= 5  # Adjust the sliding speed here
                    label.place(x=x, y=50)
                    if x > -label.winfo_width():
                        label.after(50, slide_label, label, x)
                    else:
                        root4.destroy()  # Close the window when label is out of view
                root4 = tk.Tk()
                root4.title("no valid paths")
                text = "No Valid Paths!"
                label = tk.Label(root4, text=text, font=("Arial", 40))
                label.pack()
                screen_width = root4.winfo_screenwidth()
                screen_height = root4.winfo_screenheight()
                window_width = 1000
                window_height = 300
                x_position = (screen_width - window_width) // 2
                y_position = (screen_width - window_width) // 2
                root4.geometry(f"{window_width}x{window_height}+{x_position}+{y_position}")
                root4.configure(bg = '#6F8FAF')# Start the sliding animation
                label.update()  # Make sure label dimensions are updated
                initial_x = root4.winfo_width()  # Start from the right edge of the window
                label.place(x=initial_x, y=80)
                slide_label(label, initial_x)
                root4.mainloop()
    gd = tk.Label(root2,text = 'GRID DETAILS',font=('bold italic',40),fg = '#191970', bg = '#A7C7E7')
    gd.place(x = 550 , y = 30)
    rowsl = tk.Label(root2,text ='Rows:', font = ('bold italic',30), fg = '#191970', bg = '#A7C7E7')
    rowsl.place_forget()
    rowse = tk.Entry(root2,font = ('Bold italic', 26), fg = '#191970', bg = '#A7C7E7')
    rowse.place_forget()
    coll = tk.Label(root2,text ='Columns', font = ('bold italic',30), fg = '#191970', bg = '#A7C7E7')
    coll.place_forget()
    cole = tk.Entry(root2,font = ('Bold italic', 26), fg = '#191970', bg = '#A7C7E7')
    cole.place_forget()
    ckl = tk.Label(root2,text = 'Checkpoints',font=('bold italic',30),fg = '#191970', bg = '#A7C7E7')
    ckl.place_forget()
    cke = tk.Entry(root2,font = ('Bold italic', 26), fg = '#191970', bg = '#A7C7E7')
    cke.place_forget()            
    slv = tk.Button(root2, text = 'solve',font = ('bold italic',30) , bg = '#A7C7E7', command = solve1)
    root2.after(2000,appear_rows)
    root2.after(4000,appear_columns)
    root2.after(6000,appear_ck)
    slv.place(x = 600,y = 600)
    root2.mainloop()
bttn = tk.Button(root,image = go,command = secpage,bg = 'light blue' )#borderwidth= 10 ,
bttn.place(x = 50,y = 700)
update_video()  # Start the video updating loop
root.mainloop()        